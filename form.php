<!DOCTYPE html>
<head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
?>
    <link rel="stylesheet" href="style.css ">
  </head>
  <body>
<div class="content">
    <h1 ID="da">Форма</h1>
    <form Action="" method = "POST">
      <label>
          <br />
          <input name="field-name-1" <?php if ($errors['field-name-1']) {print 'class="error"';} ?> value="<?php print $values['field-name-1'];?>" />
      </label><br />
      <label><br />
        <input name="field-name-2" 
        type="field-name-2" <?php if ($errors['field-name-2']) {print 'class="error"';} ?> value="<?php print $values['field-name-2']; ?>" />
      </label><br />

      <p>Ваш год рождения:</p>
      <label><br />
        <select name="field-name-3">
          <option value="1999" <?php if ($values['field-name-3']==1999)
            {print 'checked';}?>>1999</option>
          <option value="2000" <?php if ($values['field-name-3']==2000)
            {print 'checked';}?>>2000</option>
          <option value="2001" <?php if ($values['field-name-3']==2001)
            {print 'checked';}?>>2001</option>
          <option value="2002" selected="selected" <?php if ($values['field-name-3']==2002)
            {print 'checked';}?>>2002</option>
        </select>
      </label><br />

      <p>Пол:</p><br />
      <label><input type="radio" 
        name="radio-group-1" value="1" <?php if ($errors['radio-group-1']) {print 'class="error"';} ?> <?php if ($values['radio-group-1']==1){print 'checked';}?> />
       М</label>
      <label><input type="radio"
        name="radio-group-1" value="2" <?php if ($errors['radio-group-1']) {print 'class="error"';} ?> <?php if ($values['radio-group-1']==2){print 'checked';}?>/>
        Ж</label><br />

        <p>Кол-во конечностей:</p><br />
      <label><input type="radio" 
        name="radio-group-iq" value="0" <?php if ($errors['radio-group-iq']) {print 'class="error"';} ?> <?php if ($values['radio-group-iq']==0){print 'checked';}?>/>
        1</label>
      <label><input type="radio"
        name="radio-group-iq" value="1" <?php if ($errors['radio-group-iq']) {print 'class="error"';} ?> <?php if ($values['radio-group-iq']==1){print 'checked';}?>/>
        2</label>
      <label><input type="radio"
        name="radio-group-iq" value="2" <?php if ($errors['radio-group-iq']) {print 'class="error"';} ?> <?php if ($values['radio-group-iq']==2){print 'checked';}?>/>
        3</label>
      <label><input type="radio"
        name="radio-group-iq" value="3" <?php if ($errors['radio-group-iq']) {print 'class="error"';} ?> <?php if ($values['radio-group-iq']==3){print 'checked';}?>/>
        4</label><br />

        <p>  Сверхспособности:</p>
        <label>
            <br />
            <select name="field-name-talents[]"
              multiple="multiple">
              <option value="1" <?php $mas=str_split($values['field-name-talents']); 
              foreach($mas as $lol)
              if ($lol==1) print 'selected'; ?> <?php if ($errors['field-name-talents']) {print 'class="error"';} ?> >Мега мозг</option>
              <option value="2" <?php $mas=str_split($values['field-name-talents']); 
              foreach($mas as $lol)
              if ($lol==2) print 'selected'; ?> <?php if ($errors['field-name-talents']) {print 'class="error"';} ?> >Супер сила</option>
              <option value="3" <?php $mas=str_split($values['field-name-talents']); 
              foreach($mas as $lol)
              if ($lol==3) print 'selected'; ?> <?php if ($errors['field-name-talents']) {print 'class="error"';} ?> >Телепортация</option>
              <option value="4" <?php $mas=str_split($values['field-name-talents']); 
              foreach($mas as $lol)
              if ($lol==4) print 'selected'; ?> <?php if ($errors['field-name-talents']) {print 'class="error"';} ?> >Сверх скорость</option>
            </select>
          </label><br />

        <p>Биография:</p>
        <label><br />
        <textarea name="field-name-4" <?php if ($errors['field-name-4']) {print 'class="error"';} ?> > <?php print $values['field-name-4']; ?> </textarea>
        </label><br />

        <p>Даю согласие на обратоку данных</p><br />
        <label><input type="checkbox" checked="checked"
        name="check-1" <?php if ($errors['check-1']) {print 'class="error"';} ?> />
        Подпись</label><br />

        <input type="submit" value="Отправить" />
    </form>
</div>
<br/>
  </body>
</html>

